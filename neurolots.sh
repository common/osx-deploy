#!/bin/bash -e

# For bundles to work this code must be used before instantiating QApplication
#
##include<QDir>
#if defined(Q_OS_MAC)
#QDir dir( QFileInfo( argv[0] ).dir( )); // e.g. appdir/Contents/MacOS/appname
#dir.cdUp( );
#QCoreApplication::addLibraryPath(
#    dir.absolutePath( ) + QString( "/Plugins" ));
##endif
#

NEUROLOTS_BRANCH=master
SRC_DIR=~/tmp/neurolots
BUNDLE_DEST_DIR=~/tmp/neurolots_bundle
QT_CLANG_DIR=~/Qt/5.9.1/clang_64/
GLUT_LIB=/usr/local/Cellar/freeglut/2.8.1/lib/libglut.dylib
BUILD_OPTIONALS_SUBPROJECTS=1

git clone --recursive git@gitlab.gmrv.es:nsviz/neurolots.git ${SRC_DIR}
cd ${SRC_DIR}
git checkout ${NEUROLOTS_BRANCH}


if [[ -n ${BUILD_OPTIONALS_SUBPROJECTS} ]]; then
    if [[ -f .gitsubprojects ]]; then
        ORIG_FILE=.gitsubprojects.orig.ci
        mv .gitsubprojects ${ORIG_FILE}
        head -1 ${ORIG_FILE} > .gitsubprojects
        grep git_subproject ${ORIG_FILE} | sed -e "s/#//" >> .gitsubprojects
    fi
fi

mkdir -p Release && cd Release


export PATH=$PATH:${QT_CLANG_DIR}:${QT_CLANG_DIR}/bin

cmake .. -GNinja -DGLUT_glut_LIBRARY=${GLUT_LIB} -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=install -DCLONE_SUBPROJECTS=ON -DCOMMON_SOURCE_DIR=`pwd`/../commonsrc

ninja install

macdeployqt install/bin/NeuroTessMesh.app -dmg

cd install

appsAndLibs="bin/NeuroTessMesh.app/Contents/MacOS/NeuroTessMesh bin/NeuroTessMesh.app/Contents/Plugins/platforms/libqcocoa.dylib bin/NeuroTessMesh.app/Contents/Plugins/platforms/libqminimal.dylib bin/NeuroTessMesh.app/Contents/Plugins/platforms/libqoffscreen.dylib"

for input in $appsAndLibs; do
    for i in QtCore QtGui QtWidgets QtOpenGL QtPrintSupport; do
        install_name_tool -change @rpath/$i.framework/Versions/5/$i @executable_path/../Frameworks/$i.framework/Versions/5/$i $input
    done
done


mkdir -p ${BUNDLE_DEST_DIR}
cp -rp bin/NeuroTessMesh.app ${BUNDLE_DEST_DIR}

#TODO Add icon
